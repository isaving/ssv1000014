//Version: v0.0.1
package services

import (
	"git.forms.io/isaving/models"
	"git.forms.io/isaving/sv/ssv1000014/constant"
	constant2 "git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/legobank/legoapp/services"
	dtsClient "git.forms.io/universe/dts/client"
	"git.forms.io/universe/solapp-sdk/log"
	"time"
)

var Ssv1000014Compensable = dtsClient.Compensable{
	TryMethod:     "TrySsv1000014",
	ConfirmMethod: "ConfirmSsv1000014",
	CancelMethod:  "CancelSsv1000014",
}

type Ssv1000014 interface {
	TrySsv1000014(*models.SSV1000014I) (*models.SSV1000014O, error)
	ConfirmSsv1000014(*models.SSV1000014I) (*models.SSV1000014O, error)
	CancelSsv1000014(*models.SSV1000014I) (*models.SSV1000014O, error)
}

type Ssv1000014Impl struct {
	services.CommonTCCService
	//TODO ADD Service Self Define Field
	Ssv100012O     *models.SSV1000012O
	Ssv100012I     *models.SSV1000012I
	Ssv900004O     *models.SSV9000004O
	Ssv900004I     *models.SSV9000004I
	Ssv100022O     *models.SSV1000022O
	Ssv100022I     *models.SSV1000022I
	Ssv100017O     *models.SSV1000017O
	Ssv100017I     *models.SSV1000017I
	Ssv100016O     *models.SSV1000016O
	Ssv100016I     *models.SSV1000016I
	Ssv100014O     *models.SSV1000014O
	Ssv100014I     *models.SSV1000014I
	InContInfo     *models.SSV9000004ORecord //收款方合约信息
	OutContInfo    *models.SSV9000004ORecord //付款方合约信息
	PayAccBalance  float64
	PayAmtAvb      float64
	CltAccBalance  float64
	CltAmtAvb      float64
	SrcbizseqNoOut string
	SrcbizseqNoIn  string
	SrcBizSeqNo    string
}

// @Desc Ssv1000014 process
// @Author
// @Date 2020-12-12
func (impl *Ssv1000014Impl) TrySsv1000014(ssv1000014I *models.SSV1000014I) (ssv1000014O *models.SSV1000014O, err error) {

	impl.Ssv100014I = ssv1000014I
	//强制寻址标志
	if impl.SrcAppProps["_is_need_lookup"] != "" {
		impl.SrcAppProps["_is_need_lookup"] = "1"
	}
	impl.SrcBizSeqNo = impl.SrcAppProps[constant2.SRCBIZSEQNO]

	//判断付款双方是否都为内部户
	if impl.Ssv100014I.SourceFunds == "2" && impl.Ssv100014I.GoingFunds == "2" {
		return nil, errors.New("Transfer from internal account to internal account is not supported", constant.ERRCODE01)
	}

	//如果双方都是行内主账户，需要验证币种和钞汇标识
	if (impl.Ssv100014I.SourceFunds == "1" || impl.Ssv100014I.SourceFunds == "3") && (impl.Ssv100014I.GoingFunds == "1" || impl.Ssv100014I.GoingFunds == "3") {
		if impl.Ssv100014I.PayCUR != impl.Ssv100014I.CltCUR {
			return nil, errors.New("The currencies of the two sides are different", constant.ERRCODE14)
		}

		if impl.Ssv100014I.CashTranFlag != impl.Ssv100014I.CltCashTranFlag {
			return nil, errors.New("The cash  flag of the two sides are different", constant.ERRCODE15)
		}

	}

	//付款方处理,若果是内部户不用处理
	if impl.Ssv100014I.SourceFunds == "1" || impl.Ssv100014I.SourceFunds == "3" {
		//验密，首先查询合约
		ContInfo, err := impl.GetContInfo("O")
		if err != nil {
			return nil, err
		}
		impl.OutContInfo = ContInfo

		//如果是否需要验证密码 = Y
		if impl.Ssv100014I.NeedPswFlg == "Y" {
			//如果支取方式为  0-密码
			if ContInfo.WdrwlMthd == "0" {
				//判断上游传入的密码是否为空
				if impl.Ssv100014I.AccPsw == "" {
					return nil, errors.New("PassWord can not be null", constant.ERRCODE02)
				}

				//判断合约查询出来的密码是否为空
				if ContInfo.AccPsw == "" {
					return nil, errors.New("No seting password", constant.ERRCODE03)
				}

				//调用存款支付密码验证服务(不报错则验证通过)
				if err := impl.VerifyPassword(ContInfo.AccPsw); err != nil {
					return nil, errors.New("Verify Password failed", constant.ERRCODE04)
				}

			}
		}
		//调用客户信息查询，检查客户状态是否正常
		//客户状态 0-正常 1-死亡 2-破产 3-暂禁 4-待生效 9-注销
		ssv100012O, err := impl.QueryCustInfo(impl.Ssv100014I.PayCusId)
		if err != nil {
			log.Errorf("errors:%v", err)
			return nil, errors.New("Query customer information failed", constant.ERRCODE05)
		}
		if ssv100012O.CustStatus != "0" {
			return nil, errors.New("Customer status error", constant.ERRCODE06)
		}

		//限额查询

		//检查合约状态
		//合约状态  0-正常;1-销户计息;2-结清;3-解约-
		if ContInfo.AgreementStatus != "0" {
			return nil, errors.New("Contract status error", constant.ERRCODE07)
		}

		//检查冻结状态
		//冻结状态  0-正常;1-合约冻结;2-金额冻结;3-暂禁
		if ContInfo.FreezeType != "0" && ContInfo.FreezeType != "2" {
			return nil, errors.New("Frozen status error", constant.ERRCODE08)
		}

		//借贷记控制标志检查
		//借贷记控制标志    N-正常;D-只借记;C-只贷记;F-不允许借贷记
		if ContInfo.DepcreFlag == "C" || ContInfo.DepcreFlag == "F" {
			return nil, errors.New("Debit and credit mark error", constant.ERRCODE10)
		}

	}

	//转入方处理  如果是内部户则不做处理
	if impl.Ssv100014I.GoingFunds == "1" || impl.Ssv100014I.GoingFunds == "3" {
		//调用客户信息查询，检查客户状态是否正常
		//客户状态 0-正常 1-死亡 2-破产 3-暂禁 4-待生效 9-注销
		ssv100012O, err := impl.QueryCustInfo(impl.Ssv100014I.CltCusId)
		log.Errorf("error:%v", err)
		if err != nil {
			return nil, errors.New("Query customer information failed", constant.ERRCODE05)
		}
		if ssv100012O.CustStatus != "0" {
			return nil, errors.New("Customer status error", constant.ERRCODE06)
		}

		//限额查询

		//查询合约
		ContInfo, err := impl.GetContInfo("I")
		if err != nil {
			log.Errorf("error:%v", err)
			return nil, err
		}
		impl.InContInfo = ContInfo

		//检查合约状态
		//合约状态  0-正常;1-销户计息;2-结清;3-解约-
		if ContInfo.AgreementStatus != "0" {
			return nil, errors.New("Contract status error", constant.ERRCODE07)
		}

		//检查冻结状态
		//冻结状态  0-正常;1-合约冻结;2-金额冻结;3-暂禁
		if ContInfo.FreezeType == "3" {
			return nil, errors.New("Frozen status error", constant.ERRCODE08)
		}

		//检查借贷记状态
		//借贷记控制标志    N-正常;D-只借记;C-只贷记;F-不允许借贷记
		if ContInfo.DepcreFlag == "D" || ContInfo.DepcreFlag == "F" {
			return nil, errors.New("Debit and credit mark error", constant.ERRCODE10)
		}

	}

	if impl.Ssv100014I.SourceFunds == "1" || impl.Ssv100014I.SourceFunds == "3" {
		//存款扣款处理
		if err := impl.DepositDeduction(); err != nil {
			log.Errorf("error:%v", err)
			return nil, err
		}
		impl.PayAccBalance = impl.Ssv100016O.AmtCurrent
		impl.PayAmtAvb = impl.Ssv100016O.AmtAvaliable
		impl.SrcbizseqNoOut = impl.SrcAppProps[constant2.SRCBIZSEQNO]
	}

	if impl.Ssv100014I.GoingFunds == "1" || impl.Ssv100014I.GoingFunds == "3" {
		//存款存入处理
		if err := impl.DepositTransferIn(); err != nil {
			log.Errorf("error:%v", err)
			return nil, err
		}
		impl.CltAccBalance = impl.Ssv100017O.AmtCurrent
		impl.CltAmtAvb = impl.Ssv100017O.AmtAvaliable
		impl.SrcbizseqNoIn = impl.SrcAppProps[constant2.SRCBIZSEQNO]

		//存入冻结状态处理服务
		if err := impl.FreezHandle(); err != nil {
			return nil, err
		}
	}

	//新增转账交易明细
	custId := ""
	if impl.Ssv100014I.SourceFunds != "2" {
		custId = impl.Ssv100014I.PayCusId
	} else {
		custId = impl.Ssv100014I.CltCusId
	}
	if err = impl.InserTranDetail(custId); err != nil {
		log.Debug("Insert tran detail failed")
		return nil, err
	}

	//如果两边都是合约，在新增一次交易明细（双方不在同一个dcn）
	if (impl.Ssv100014I.SourceFunds == "1" || impl.Ssv100014I.SourceFunds == "3") && (impl.Ssv100014I.GoingFunds == "1" || impl.Ssv100014I.GoingFunds == "3") {
		if err = impl.InserTranDetail(impl.Ssv100014I.CltCusId); err != nil {
			log.Debug("Insert tran detail failed")
			if errors.GetErrorCode(err) == "SV77000004" {
				//主键重复 不做处理
			} else {
				return nil, err
			}

		}
	}

	ssv1000014O = &models.SSV1000014O{
		TueTranAmt: impl.Ssv100014I.TranAmt, //实际扣款金额
		ReturnCode: "0",
	}

	return ssv1000014O, nil
}

//存入冻结状态处理服务
func (impl *Ssv1000014Impl) FreezHandle() error {
	ssv9000025I := models.SV900025I{
		Agreement:     impl.Ssv100014I.CltAgrmt,    //合约号
		AgreementType: impl.Ssv100014I.CltAgrmtTyp, //合约类型
		AmtCurrent:    impl.CltAccBalance,          //当前余额
	}
	//pack request
	reqBody, err := ssv9000025I.PackRequest()
	if nil != err {
		return err
	}
	//call das get serial no
	resBody, err := impl.RequestSyncServiceElementKey(constant.DLS_TYPE_AGM, ssv9000025I.Agreement, constant.SV900025, reqBody)
	if err != nil {
		return err
	}

	ssv900025O := models.SV900025O{}
	err = ssv900025O.UnPackResponse(resBody)
	if err != nil {
		return err
	}

	return nil
}

//存款扣款处理
func (impl *Ssv1000014Impl) DepositDeduction() error {
	ssv1000016I := models.SSV1000016I{
		CustId:     impl.OutContInfo.CstmrId,                //客户号
		ContractId: impl.OutContInfo.AgreementID,            //合约号
		Account:    impl.OutContInfo.AccAcount,              //核算账号
		Amount:     impl.Ssv100014I.TranAmt,                 //交易金额
		TrnDate:    time.Now().Format("2006-01-02"),         //业务日期
		TrnSeq:     impl.SrcAppProps[constant2.SRCBIZSEQNO], //业务流水
		FlowSeq:    1,
	}
	//pack request
	reqBody, err := ssv1000016I.PackRequest()
	if nil != err {
		return err
	}
	//call das get serial no
	resBody, err := impl.RequestSyncServiceElementKey(constant.DLS_TYPE_CMM, constant.DLS_TYPE_ELEMENT_ID, constant.SV100016, reqBody)
	if err != nil {
		return err
	}

	ssv100016O := models.SSV1000016O{}
	err = ssv100016O.UnPackResponse(resBody)
	if err != nil {
		return err
	}

	impl.Ssv100016O = &ssv100016O

	return nil
}

//新增转账交易明细
func (impl *Ssv1000014Impl) InserTranDetail(custId string) error {
	sv900023I := models.SV900023I{
		TransationDate:          time.Now().Format("2006-01-02"),
		TransationTime:          time.Now().Format("15:03:04"),
		GlobalBizSeqNo:          impl.SrcAppProps[constant2.GLOBALBIZSEQNO],
		SrcBizSeqNo:             impl.SrcBizSeqNo,
		SrcTimeStamp:            impl.SrcAppProps[constant2.SRCTIMESTAMP],
		SrcSysId:                impl.SrcAppProps[constant2.SRCSYSID],
		SrcDcn:                  impl.SrcAppProps[constant2.SRCDCN],
		OrgChannelType:          impl.SrcAppProps[constant2.ORGCHANNELTYPE],
		TxDeviceId:              impl.SrcAppProps[constant2.TXDEVICEID],
		TxDeptCode:              impl.SrcAppProps[constant2.TXDEPTCODE],
		BnAcc:                   "",
		TransationEm:            "",
		TransationType:          impl.Ssv100014I.TranTyp,
		PaySourceFunds:          impl.Ssv100014I.SourceFunds,
		PayCusid:                impl.Ssv100014I.PayCusId,
		PayCusty:                impl.Ssv100014I.PayCusTy,
		PayPrductId:             "",
		PayPrductNm:             0,
		PayAgreement:            impl.Ssv100014I.PayAgrmt,
		PayAgreementType:        impl.Ssv100014I.PayAgrmtTyp,
		PayMdsNm:                impl.Ssv100014I.PayMdsNM,
		PayMdsTyp:               impl.Ssv100014I.PayMdsTyp,
		PayCurrency:             impl.Ssv100014I.PayCUR,
		PayCashtranFlag:         impl.Ssv100014I.CashTranFlag,
		PayAccBalance:           impl.PayAccBalance,
		PayAccAvb:               impl.PayAmtAvb,
		PayUscode:               impl.Ssv100014I.UsgCod,
		PayAccuntNme:            impl.Ssv100014I.PayAccuntNme,
		PayBizSn:                impl.SrcbizseqNoOut,
		CltSourceFunds:          impl.Ssv100014I.GoingFunds,
		CltCusid:                impl.Ssv100014I.CltCusId,
		CltCusty:                impl.Ssv100014I.CltCusTy,
		CltPrductId:             "",
		CltPrductNm:             0,
		CltAgreement:            impl.Ssv100014I.CltAgrmt,
		CltAgreementType:        impl.Ssv100014I.CltAgrmtTyp,
		CltMdsNm:                impl.Ssv100014I.CltMdsNM,
		CltMdsTyp:               impl.Ssv100014I.CltMdsTyp,
		CltCurrency:             impl.Ssv100014I.CltCUR,
		CltCashtranFlag:         impl.Ssv100014I.CltCashTranFlag,
		CltAccBalance:           impl.CltAccBalance,
		CltAccAvb:               impl.CltAmtAvb,
		CltAccuntNme:            impl.Ssv100014I.CltAccuntNme,
		CltUscode:               impl.Ssv100014I.CltUsgCod,
		CltBizSn:                impl.SrcbizseqNoIn,
		TransationAmt:           impl.Ssv100014I.TranAmt,
		Postscript:              impl.Ssv100014I.Postscript,
		PairNm:                  "",
		BatchNm:                 impl.Ssv100014I.BatchNm,
		TradeFlag:               "N",
		ReverseTradeFlag:        "N",
		OriginalTransactionDate: "",
		OriginalGlobalBizseqNo:  "",
		OriginalSrcBizseqNo:     "",
		TranResult:              "N",
		ErrorCode:               "",
		ErrorDesc:               "",
		LastUpDatetime:          time.Now().Format("2006-01-02 15:04:05"),
		LastUpBn:                impl.SrcAppProps[constant2.TXDEPTCODE],
		LastUpEm:                impl.SrcAppProps[constant2.TXDEVICEID],
	}
	if impl.Ssv100014I.SourceFunds == "1" || impl.Ssv100014I.SourceFunds == "3" {
		sv900023I.PayPrductId = impl.OutContInfo.PrductId
		sv900023I.PayPrductNm = impl.OutContInfo.PrductNm
	}
	if impl.Ssv100014I.GoingFunds == "1" || impl.Ssv100014I.GoingFunds == "3" {
		sv900023I.CltPrductId = impl.InContInfo.PrductId
		sv900023I.CltPrductNm = impl.InContInfo.PrductNm
	}

	//pack request
	reqBody, err := sv900023I.PackRequest()
	if nil != err {
		return err
	}
	//call das get serial no
	resBody, err := impl.RequestSyncServiceElementKey(constant.DLS_TYPE_CUS, custId, constant.SV900023, reqBody)
	if err != nil {
		return err
	}

	ssv900004O := models.SSV9000004O{}
	err = ssv900004O.UnPackResponse(resBody)
	if err != nil {
		return err
	}

	return nil

}

//合约信息查询
func (impl *Ssv1000014Impl) QueryContInfo(CustId string, ssv9004I *models.SSV9000004I) (*models.SSV9000004ORecord, error) {

	//pack request
	reqBody, err := ssv9004I.PackRequest()
	if nil != err {
		return nil, err
	}
	//call das get serial no
	resBody, err := impl.RequestSyncServiceElementKey(constant.DLS_TYPE_CUS, CustId, constant.SV900004, reqBody)
	if err != nil {
		return nil, err
	}

	ssv900004O := models.SSV9000004O{}
	err = ssv900004O.UnPackResponse(resBody)
	if err != nil {
		return nil, err
	}
	if len(ssv900004O.Records) == 0 {
		return nil, errors.New("Record not found", constant.ERRCODE06)
	}

	return &ssv900004O.Records[0], nil

}

//获取合约信息
func (impl *Ssv1000014Impl) GetContInfo(Flag string) (*models.SSV9000004ORecord, error) {
	var queryType, MdiaType, MdiaId, CustId, ContType, ContId, Currency, CashTranFlag, UsgCod string
	//I代表转入方
	if Flag == "I" {
		queryType = impl.Ssv100014I.GoingFunds
		MdiaType = impl.Ssv100014I.CltMdsTyp
		MdiaId = impl.Ssv100014I.CltMdsNM
		CustId = impl.Ssv100014I.CltCusId
		ContType = impl.Ssv100014I.CltAgrmtTyp
		ContId = impl.Ssv100014I.CltAgrmt
		Currency = impl.Ssv100014I.CltCUR
		CashTranFlag = impl.Ssv100014I.CltCashTranFlag
		UsgCod = impl.Ssv100014I.CltUsgCod

		//O代表转出方
	} else if Flag == "O" {
		queryType = impl.Ssv100014I.SourceFunds
		MdiaType = impl.Ssv100014I.PayMdsTyp
		MdiaId = impl.Ssv100014I.PayMdsNM
		CustId = impl.Ssv100014I.PayCusId
		ContType = impl.Ssv100014I.PayAgrmtTyp
		ContId = impl.Ssv100014I.PayAgrmt
		Currency = impl.Ssv100014I.PayCUR
		CashTranFlag = impl.Ssv100014I.CashTranFlag
		UsgCod = impl.Ssv100014I.UsgCod
	}

	if queryType == "1" {
		//通过介质查询合约信息,介质类型和介质号码不能为空
		if MdiaType == "" || MdiaId == "" {
			return nil, errors.New("Media type and Media number can not be null", constant.ERRCODE12)
		}
		ContInfo := &models.SSV9000004I{
			QryTpy:       "2",      //查询类型  0-介质  1-合约
			MediaType:    MdiaType, //介质类型
			MediaNm:      MdiaId,   //介质号码
			Currency:     Currency,
			CashTranFlag: CashTranFlag,
			ChannelNm:    impl.SrcAppProps[constant2.ORGCHANNELTYPE],
			UsgCod:       UsgCod,
		}
		sv90004O, err := impl.QueryContInfo(CustId, ContInfo)
		if err != nil {
			return nil, errors.New("Query contract information failed", constant.ERRCODE13)
		}
		return sv90004O, nil
	} else {
		//通过合约号查询合约信息，合约号和合约类型不能为空
		if ContType == "" || ContId == "" {
			return nil, errors.New("Agreement type and Agreement number can not be null", constant.ERRCODE11)
		}
		ContInfo := &models.SSV9000004I{
			QryTpy:        "1",      //查询类型  0-介质  1-合约
			AgreementId:   ContId,   //合约号
			AgreementType: ContType, //合约类型
		}
		sv90004O, err := impl.QueryContInfo(CustId, ContInfo)
		if err != nil {
			return nil, errors.New("Query contract information failed", constant.ERRCODE13)
		}
		return sv90004O, nil
	}

}

//客户信息查询
func (impl *Ssv1000014Impl) QueryCustInfo(CustId string) (*models.SSV1000012O, error) {
	ssv1000012I := models.SSV1000012I{
		CustId: CustId,
	}
	//pack request
	reqBody, err := ssv1000012I.PackRequest()
	if nil != err {
		return nil, err
	}
	//call das get serial no
	resBody, err := impl.RequestSyncServiceElementKey(constant.DLS_TYPE_CMM, constant.DLS_TYPE_ELEMENT_ID, constant.SV100012, reqBody)
	if err != nil {
		return nil, err
	}

	ssv100012O := models.SSV1000012O{}
	err = ssv100012O.UnPackResponse(resBody)
	if err != nil {
		return nil, err
	}

	return &ssv100012O, nil
}

//验证密码
func (impl *Ssv1000014Impl) VerifyPassword(passWord string) error {

	ssv1000022I := models.SSV1000022I{
		KeyVersion:    impl.Ssv100014I.KeyVersion, //公钥版本号
		BeVerifiedPsw: impl.Ssv100014I.AccPsw,     //待验证支付密码
		AgrPsw:        passWord,                   //合约支付密码
	}
	//pack request
	reqBody, err := ssv1000022I.PackRequest()
	if nil != err {
		return err
	}
	//call das get serial no
	resBody, err := impl.RequestSyncServiceElementKey(constant.DLS_TYPE_CMM, constant.DLS_TYPE_ELEMENT_ID, constant.SV100022, reqBody)
	if err != nil {
		return err
	}

	ssv100022O := models.SSV1000022O{}
	err = ssv100022O.UnPackResponse(resBody)
	if err != nil {
		return err
	}

	return nil
}

//存款存入处理
func (impl *Ssv1000014Impl) DepositTransferIn() error {
	ssv1000017I := models.SSV1000017I{
		CustId:     impl.InContInfo.CstmrId,                 //客户号
		ContractId: impl.InContInfo.AgreementID,             //合约号
		Account:    impl.InContInfo.AccAcount,               //核算账号
		Amount:     impl.Ssv100014I.TranAmt,                 //交易金额
		TrnDate:    time.Now().Format("2006-01-02"),         //业务日期
		TrnSeq:     impl.SrcAppProps[constant2.SRCBIZSEQNO], //业务流水
		FlowSeq:    1,
	}
	//pack request
	reqBody, err := ssv1000017I.PackRequest()
	if nil != err {
		return err
	}
	//call das get serial no
	resBody, err := impl.RequestSyncServiceElementKey(constant.DLS_TYPE_CMM, constant.DLS_TYPE_ELEMENT_ID, constant.SV100017, reqBody)
	if err != nil {
		return err
	}

	ssv100017O := models.SSV1000017O{}
	err = ssv100017O.UnPackResponse(resBody)
	if err != nil {
		return err
	}
	impl.Ssv100017O = &ssv100017O

	return nil
}

func (impl *Ssv1000014Impl) ConfirmSsv1000014(ssv1000014I *models.SSV1000014I) (ssv1000014O *models.SSV1000014O, err error) {
	//TODO Business  confirm Process
	log.Debug("Start confirm ssv1000014")
	return nil, nil
}

func (impl *Ssv1000014Impl) CancelSsv1000014(ssv1000014I *models.SSV1000014I) (ssv1000014O *models.SSV1000014O, err error) {
	//TODO Business  cancel Process
	log.Debug("Start cancel ssv1000014")
	return nil, nil
}
