//Version: v0.0.1
package services

import (
	"git.forms.io/isaving/models"
	"testing"
)

var sv900004 = `{
    "errorCode": "0",
    "errorMsg": "success",
    "response": {
        "RecordTotNum": 1,
        "Records": [
            {
                "AgreementID": "1",
                "AgreementType": "1",
                "Currency": "1",
                "CashtranFlag": "1",
                "AgreementStatus": "0",
                "AccOpnDt": "1",
                "CstmrCntctPh": "1",
                "CstmrCntctAdd": "1",
                "CstmrCntctEm": "1",
                "AccPsw": "1",
                "WdrwlMthd": "0",
                "AccAcount": "1",
                "CstmrId": "1",
                "FreezeType": "0",
                "TmpryPrhibition": "0",
                "DepcreFlag": "N",
                "AccuntNme": "1"
            }
        ]
    }
}`

var sv100012 = `{
    "errorCode": "0",
    "errorMsg": "success",
    "response": {
        "CustStatus":"0"
    }
}`

var sv100016 = `{
    "errorCode": "0",
    "errorMsg": "success",
    "response": {
        "AmtFreezing":1,
		"AmtCurrent":1,
		"AmtLast":1,
		"AmtAvaliable":1,
		"AccStatus":"1"
    }
}`

var sv100017 = `{
    "errorCode": "0",
    "errorMsg": "success",
    "response": {
        "AmtFreezing":1,
		"AmtCurrent":1,
		"AmtLast":1,
		"AmtAvaliable":1,
		"AccStatus":"1"
    }
}`

var sv100022 = `{
    "errorCode": "0",
    "errorMsg": "success",
    "response": {
        "State":"ok"
    }
}`

var SV900023 = `{
    "errorCode": "0",
    "errorMsg": "success",
    "response": {
        
    }
}`

func (this *Ssv1000014Impl) RequestSyncServiceElementKey(elementType, elementId, serviceKey string, requestData []byte) (responseData []byte, err error) {
	switch serviceKey {
	case "sv900004":
		responseData = []byte(sv900004)
	case "sv100012":
		responseData = []byte(sv100012)
	case "sv100016":
		responseData = []byte(sv100016)
	case "sv100017":
		responseData = []byte(sv100017)
	case "sv100022":
		responseData = []byte(sv100022)
	case "SV900023":
		responseData = []byte(SV900023)
	}

	return responseData, nil
}

func TestSsv1000016Impl_Ssv1000016(t *testing.T) {

	Ssv10000141Impl := new(Ssv1000014Impl)

	//正向流程  资金来源 = 1  资金去向 = 1
	_, _ = Ssv10000141Impl.TrySsv1000014(&models.SSV1000014I{
		SourceFunds:     "1",
		PayAgrmt:        "1",
		PayAgrmtTyp:     "1",
		PayInAcc:        "1",
		PayMdsNM:        "1",
		PayMdsTyp:       "1",
		PayCusId:        "1",
		PayCUR:          "1",
		CashTranFlag:    "1",
		PayAccuntNme:    "1",
		UsgCod:          "1",
		WdrwlMthd:       "0",
		NeedPswFlg:      "Y",
		AccPsw:          "1",
		TranAmt:         0,
		GoingFunds:      "1",
		CltAgrmt:        "1",
		CltAgrmtTyp:     "1",
		CltInAcc:        "1",
		CltMdsNM:        "1",
		CltMdsTyp:       "1",
		CltCusId:        "1",
		CltCUR:          "1",
		CltAccuntNme:    "1",
		CltCashTranFlag: "1",
		Postscript:      "1",
		DlyArrvlAFlag:   "1",
		TranTyp:         "1",
		AmtFreAuth:      "1",
		KeyVersion:      "1",
	})

	//正向流程  资金来源 = 3  资金去向 = 3
	_, _ = Ssv10000141Impl.TrySsv1000014(&models.SSV1000014I{
		SourceFunds:     "3",
		PayAgrmt:        "1",
		PayAgrmtTyp:     "1",
		PayInAcc:        "1",
		PayMdsNM:        "1",
		PayMdsTyp:       "1",
		PayCusId:        "1",
		PayCUR:          "1",
		CashTranFlag:    "1",
		PayAccuntNme:    "1",
		UsgCod:          "1",
		WdrwlMthd:       "0",
		NeedPswFlg:      "1",
		AccPsw:          "1",
		TranAmt:         0,
		GoingFunds:      "3",
		CltAgrmt:        "1",
		CltAgrmtTyp:     "1",
		CltInAcc:        "1",
		CltMdsNM:        "1",
		CltMdsTyp:       "1",
		CltCusId:        "1",
		CltCUR:          "1",
		CltAccuntNme:    "1",
		CltCashTranFlag: "1",
		Postscript:      "1",
		DlyArrvlAFlag:   "1",
		TranTyp:         "1",
		AmtFreAuth:      "1",
		KeyVersion:      "1",
	})

	//报错流程  资金来源 = 2  资金去向 = 2
	_, _ = Ssv10000141Impl.TrySsv1000014(&models.SSV1000014I{
		SourceFunds:     "2",
		PayAgrmt:        "1",
		PayAgrmtTyp:     "1",
		PayInAcc:        "1",
		PayMdsNM:        "1",
		PayMdsTyp:       "1",
		PayCusId:        "1",
		PayCUR:          "1",
		CashTranFlag:    "1",
		PayAccuntNme:    "1",
		UsgCod:          "1",
		WdrwlMthd:       "0",
		NeedPswFlg:      "1",
		AccPsw:          "1",
		TranAmt:         0,
		GoingFunds:      "2",
		CltAgrmt:        "1",
		CltAgrmtTyp:     "1",
		CltInAcc:        "1",
		CltMdsNM:        "1",
		CltMdsTyp:       "1",
		CltCusId:        "1",
		CltCUR:          "1",
		CltAccuntNme:    "1",
		CltCashTranFlag: "1",
		Postscript:      "1",
		DlyArrvlAFlag:   "1",
		TranTyp:         "1",
		AmtFreAuth:      "1",
		KeyVersion:      "1",
	})

	_, _ = Ssv10000141Impl.ConfirmSsv1000014(&models.SSV1000014I{})
	_, _ = Ssv10000141Impl.CancelSsv1000014(&models.SSV1000014I{})

}
