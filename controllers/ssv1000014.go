//Version: v0.0.1
package controllers

import (
	"git.forms.io/isaving/models"
	"git.forms.io/isaving/sv/ssv1000014/services"
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/controllers"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/dts/client/aspect"
	"git.forms.io/universe/solapp-sdk/log"
	"runtime/debug"
)

type Ssv1000014Controller struct {
	controllers.CommTCCController
}

func (*Ssv1000014Controller) ControllerName() string {
	return "Ssv1000014Controller"
}

// @Desc ssv1000014 controller
// @Author
// @Date 2020-12-12
func (c *Ssv1000014Controller) Ssv1000014() {

	defer func() {
		if r := recover(); r != nil {
			err := errors.Errorf(constant.SYSPANIC, "Ssv1000014Controller.Ssv1000014 Catch panic %v", r)
			log.Errorf("Error: %v, Stack: [%s]", err, debug.Stack())
			c.SetServiceError(err)
		}
	}()

	ssv1000014I := &models.SSV1000014I{}
	if err := models.UnPackRequest(c.Req.Body, ssv1000014I); err != nil {
		c.SetServiceError(err)
		return
	}
	if err := ssv1000014I.Validate(); err != nil {
		log.Errorf("Request message field validate failed, error:[%v]", err)
		c.SetServiceError(err)
		return
	}
	ssv1000014 := &services.Ssv1000014Impl{}
	ssv1000014.New(c.CommTCCController)
	ssv1000014.Ssv100014I = ssv1000014I
	ssv1000014Compensable := services.Ssv1000014Compensable

	proxy, err := aspect.NewDTSProxy(ssv1000014, ssv1000014Compensable, c.DTSCtx)
	if err != nil {
		log.Error("Register DTS Proxy failed. %v", err)
		c.SetServiceError(errors.Errorf(constant.PROXYREGFAILD, "Register DTS Proxy failed. %v", err))
		return
	}

	rets := proxy.Do(ssv1000014I)

	if len(rets) < 1 {
		log.Error("DTS proxy executed service failed, not have any return")
		c.SetServiceError(errors.Errorf(constant.PROXYFAILD, "DTS proxy executed service failed, %v", "not have any return"))
		return
	}

	if e := rets[len(rets)-1].Interface(); e != nil {
		log.Errorf("DTS proxy executed service failed %v", err)
		c.SetServiceError(e)
		return
	}

	rsp := rets[0].Interface()
	if ssv1000014O, ok := rsp.(*models.SSV1000014O); ok {
		if responseBody, err := models.PackResponse(ssv1000014O); err != nil {
			c.SetServiceError(err)
		} else {
			c.SetAppBody(responseBody)
		}
	}
}

// @Title Ssv1000014 Controller
// @Description ssv1000014 controller
// @Param Ssv1000014 body model.SSV1000014I true body for SSV1000014 content
// @Success 200 {object} model.SSV1000014O
// @router /create [post]
func (c *Ssv1000014Controller) SWSsv1000014() {
	//Here is to generate API documentation, no need to implement methods
}
