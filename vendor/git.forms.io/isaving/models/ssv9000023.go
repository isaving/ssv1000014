//Version: v0.0.1
package models

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/universe/common/json"
	"gopkg.in/go-playground/validator.v9"
)

type SV900023I struct {
	TransationDate          string  //交易日期
	TransationTime          string  //交易时间
	GlobalBizSeqNo          string  //全局流水号
	SrcBizSeqNo             string  //业务流水号
	SrcTimeStamp            string  //源系统工作日期时间
	SrcSysId                string  //源系统编码
	SrcDcn                  string  //源系统DCN
	OrgChannelType          string  //交易发起渠道
	TxDeviceId              string  //终端号
	TxDeptCode              string  //交易行所
	BnAcc                   string  //账务行所
	TransationEm            string  //交易柜员
	TransationType          string  //交易方式
	PaySourceFunds          string  //转出方账号类型
	PayCusid                string  //转出方客户编号
	PayCusty                string  //转出方客户类型
	PayPrductId             string  //转出方产品号
	PayPrductNm             int     //转出方产品顺序号
	PayAgreement            string  //转出方合约号
	PayAgreementType        string  //转出方合约类型
	PayMdsNm                string  //转出方介质号码
	PayMdsTyp               string  //转出方介质类型
	PayCurrency             string  //转出方币种
	PayCashtranFlag         string  //转出方钞汇标识
	PayAccBalance           float64 //转出方账户余额
	PayAccAvb               float64 //转出方可用余额',
	PayUscode               string  //转出方用途代码
	PayAccuntNme            string  //转出方账户名称
	PayBizSn                string  //转出方业务流水号
	CltSourceFunds          string  //转入方账号类型
	CltCusid                string  //转入方客户编号
	CltCusty                string  //转入方客户类型
	CltPrductId             string  //转入方产品号
	CltPrductNm             int     //转入方产品顺序号
	CltAgreement            string  //转入方合约号
	CltAgreementType        string  //转入方合约类型
	CltMdsNm                string  //转入方介质号码
	CltMdsTyp               string  //转入方介质类型
	CltCurrency             string  //转入方币种
	CltCashtranFlag         string  //转入方钞汇标识
	CltAccBalance           float64 //转入方账户余额
	CltAccAvb               float64 //转入方可用余额',
	CltAccuntNme            string  //转入方账户名称
	CltUscode               string  //转入方用途代码
	CltBizSn                string  //转入方业务流水号
	TransationAmt           float64 //交易金额
	Postscript              string  //转账附言
	PairNm                  string  //套号
	BatchNm                 string  //批号
	TradeFlag               string  //反交易标志
	ReverseTradeFlag        string  //冲正交易标志
	OriginalTransactionDate string  //原交易日期
	OriginalGlobalBizseqNo  string  //原全局流水号
	OriginalSrcBizseqNo     string  //原业务流水号
	TranResult              string  //处理状态
	ErrorCode               string  //错误码
	ErrorDesc               string  //错误描述
	LastUpDatetime          string  //最后更新日期时间
	LastUpBn                string  //最后更新行所
	LastUpEm                string  //最后更新柜员
}

type SV900023O struct {

}

// @Desc Build request message
func (o *SV900023I) PackRequest() (requestBody []byte, err error) {

	requestBody, err = json.Marshal(o)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.REQPACKERR)
	}

	return requestBody, nil
}

// @Desc Parsing request message
func (o *SV900023I) UnPackRequest(requestBody []byte) (err error) {

	if err := json.Unmarshal(requestBody, o); nil != err {
		return errors.Wrap(err, 0, constant.REQUNPACKERR)
	}

	return nil
}

// @Desc Build response message
func (o *SV900023O) PackResponse() (responseBody []byte, err error) {

	commResp := &CommonResponse{
		ReturnCode: successCode,
		ReturnMsg:  successMsg,
		Data:       o,
	}

	responseBody, err = json.Marshal(commResp)
	if err != nil {
		return nil, errors.Wrap(err, 0, constant.RSPPACKERR)
	}

	return responseBody, nil
}

// @Desc Parsing response message
func (o *SV900023O) UnPackResponse(responseBody []byte) (err error) {

	commResp := &CommonResponse{
		Data: o,
	}

	if err := json.Unmarshal(responseBody, commResp); nil != err {
		return errors.Wrap(err, 0, constant.RSPUNPACKERR)
	}

	return nil
}

func (o *SV900023I) Validate() error {

	validate := validator.New()
	return validate.Struct(o)

}

func (*SV900023I) GetServiceKey() string {
	return "SV900023"
}
