//Copyright 2019 Shenzhen Forms Syntron Information Co., Ltd. All rights reserved.

package transaction

import (
	"git.forms.io/universe/dts/common/comm"
	constant "git.forms.io/universe/dts/common/const"
	event "git.forms.io/universe/dts/common/event/transaction"
	"git.forms.io/universe/dts/common/util"
	"git.forms.io/universe/solapp-sdk/compensable"
	"git.forms.io/universe/solapp-sdk/config"
	"github.com/pkg/errors"
	uuid "github.com/satori/go.uuid"
	"strings"
)

const (
	CONFIRM_FLAG = 1 << iota
	CANCEL_FLAG
)

func IsFlag(flagSet int, flag int) bool {
	return (flagSet & flag) != 0
}

// @Desc new a TxnManager
func NewTxnManager(client comm.Client) TxnManager {
	return &DefaultTxnManager{
		RemoteClient: client,
	}
}

// @Desc a interface contains Register, Enlist, ReportTryResult to implement TCC flow
type TxnManager interface {
	Register(spanCtx *compensable.SpanContext, dtsAgentAddress string, compensableFlagSet int) (string, error)

	Enlist(spanCtx *compensable.SpanContext, dtsAgentAddress string, rootXid string, parentXid string, compensableFlagSet int) (string, error)

	ReportTryResult(spanCtx *compensable.SpanContext, dtsAgentAddress string, rootXid string, parentXid string, branchXid string, ok bool) error
}

type DefaultTxnManager struct {
	RemoteClient comm.Client
}

func generateParticipantAddress(
	isExistConfirmMethod bool,
	isExistCancelMethod bool,
	isDirectRequest bool) (participantAddress string) {
	branchConfirmAddress := ""
	branchCancelAddress := ""

	if isDirectRequest {
		//get branchConfirmAddress
		if isExistConfirmMethod {
			branchConfirmAddress = config.CmpSvrConfig.ConfirmUrlPath
		}
		//get branchCancelAddress
		if isExistCancelMethod {
			branchCancelAddress = config.CmpSvrConfig.CancelUrlPath
		}

		//make up participantAddress
		participantAddress =
			config.CmpSvrConfig.CommType + "|" +
				config.CmpSvrConfig.ParticipantAddress + "|" +
				branchConfirmAddress + "|" +
				branchCancelAddress
	} else {
		//get branchConfirmAddress
		if isExistConfirmMethod {
			branchConfirmAddress = config.CmpSvrConfig.DtsClientConfirmTopicId
		}

		//get branchCancelAddress
		if isExistCancelMethod {
			branchCancelAddress = config.CmpSvrConfig.DtsClientCancelTopicId
		}

		participantAddress =
			config.CmpSvrConfig.CommType + "|" +
				branchConfirmAddress + "|" +
				branchCancelAddress
	}

	return
}

// @Desc Register root transaction
// @Param spanCtx
// @Param dtsAgentAddress some information of dtsAgent
// @Param compensableFlagSet
// @Return rootXid transaction's rootXid
// @Return err error
func (d *DefaultTxnManager) Register(spanCtx *compensable.SpanContext, dtsAgentAddress string, compensableFlagSet int) (rootXid string, err error) {
	var participantAddress string
	dtsAgentAddressElem := strings.Split(dtsAgentAddress, constant.PARTICIPANT_ADDRESS_SPLIT_CHAR)
	isDirectRequest := strings.EqualFold(constant.COMM_DIRECT, config.CmpSvrConfig.CommType)
	isExistConfirmMethod := IsFlag(compensableFlagSet, CONFIRM_FLAG)
	isExistCancelMethod := IsFlag(compensableFlagSet, CANCEL_FLAG)

	participantAddress = generateParticipantAddress(isExistConfirmMethod, isExistCancelMethod, isDirectRequest)

	rootXid = uuid.NewV4().String()
	//make up RootTxnRegisterRequest
	request := event.RootTxnRegisterRequest{
		Head: event.TxnEventHeader{
			Service: "RegisterRootTransaction",
		},
		Request: event.RootTxnRegisterRequestBody{
			ParticipantAddress: participantAddress,
			RequestTime:        util.CurrentTime(),
			ParentXid:          rootXid,
			RootXid:            rootXid,
			BranchXid:          rootXid,
		},
	}

	//var response string
	var e error
	resBody := &event.RootTxnRegisterResponse{}
	if isDirectRequest {
		// DTS agent address + "|" + Register Path + "|" + Enlist Path + "|" + Result try report Path
		e = d.RemoteClient.HttpPost(nil, dtsAgentAddressElem[0]+dtsAgentAddressElem[1], request, resBody, nil,
			config.CmpSvrConfig.DtsTimeoutMilliseconds,
			config.CmpSvrConfig.DtsMaxRetryTimes)
	} else {
		// ORG + "|" + DCN + "|" + NODE ID + "|" + Instance ID + "|" + Register TopicID + "|" + Enlist TopicID + "|" + Result try report TopicID
		if len(dtsAgentAddressElem) != 7 {
			err = errors.Errorf("target address invalid, please check[%s]", dtsAgentAddress)
			return
		}
		topicAttributes, _ := comm.BuildDTSTopicAttributes(
			dtsAgentAddressElem[0],
			dtsAgentAddressElem[1],
			dtsAgentAddressElem[2],
			dtsAgentAddressElem[3],
			dtsAgentAddressElem[4])

		e = d.RemoteClient.DTSRequestWithEvent(
			spanCtx,
			nil,
			topicAttributes,
			request,
			resBody,
			config.CmpSvrConfig.DtsTimeoutMilliseconds,
			config.CmpSvrConfig.DtsMaxRetryTimes,
		)
	}

	if e != nil {
		err = e
		return
	}

	if 0 == resBody.ErrorCode {
		rootXid = resBody.Data.RootXid
	} else {
		err = errors.Errorf("Transaction register failed:[%s]!", resBody.ErrorMsg)
		return
	}

	return
}

// @Desc Enlist branch transaction
// @Param spanCtx
// @Param dtsAgentAddress some information of dtsAgent
// @Param compensableFlagSet
// @Param rootXid
// @Param parentXid
// @Return branchXid transaction's rootXid
// @Return err error
func (d *DefaultTxnManager) Enlist(spanCtx *compensable.SpanContext, dtsAgentAddress string, rootXid string, parentXid string, compensableFlagSet int) (branchXid string, err error) {
	var participantAddress string
	dtsAgentAddressElem := strings.Split(dtsAgentAddress, constant.PARTICIPANT_ADDRESS_SPLIT_CHAR)

	isDirectRequest := strings.EqualFold(constant.COMM_DIRECT, config.CmpSvrConfig.CommType)
	isExistConfirmMethod := IsFlag(compensableFlagSet, CONFIRM_FLAG)
	isExistCancelMethod := IsFlag(compensableFlagSet, CANCEL_FLAG)

	participantAddress = generateParticipantAddress(isExistConfirmMethod, isExistCancelMethod, isDirectRequest)

	branchXid = uuid.NewV4().String()
	request := event.BranchTxnEnlistRequest{
		Head: event.TxnEventHeader{
			Service: "EnlistBranchTransaction",
		},
		Request: event.BranchTxnEnlistRequestBody{
			ParticipantAddress: participantAddress,
			RootXid:            rootXid,
			ParentXid:          parentXid,
			BranchXid:          branchXid,
			RequestTime:        util.CurrentTime(),
		},
	}

	//var response string
	var e error
	resBody := &event.BranchTxnEnlistResponse{}
	if isDirectRequest {
		e = d.RemoteClient.HttpPost(nil, dtsAgentAddressElem[0]+dtsAgentAddressElem[2], request, resBody, nil,
			config.CmpSvrConfig.DtsTimeoutMilliseconds,
			config.CmpSvrConfig.DtsMaxRetryTimes)
	} else {
		if len(dtsAgentAddressElem) != 7 {
			err = errors.Errorf("target address invalid, please check[%++v]", dtsAgentAddress)
			return
		}

		topicAttributes, _ := comm.BuildDTSTopicAttributes(
			dtsAgentAddressElem[0],
			dtsAgentAddressElem[1],
			dtsAgentAddressElem[2],
			dtsAgentAddressElem[3],
			dtsAgentAddressElem[5])

		e = d.RemoteClient.DTSRequestWithEvent(
			spanCtx,
			nil,
			topicAttributes,
			request,
			resBody,
			config.CmpSvrConfig.DtsTimeoutMilliseconds,
			config.CmpSvrConfig.DtsMaxRetryTimes,
		)
	}

	if e != nil {
		err = e
		return
	} else {
		if 0 == resBody.ErrorCode {
			branchXid = resBody.Data.BranchXid
		} else {
			err = errors.Errorf("Transaction enlist failed:[%s]!", resBody.ErrorMsg)
			return
		}
	}

	return
}

// @Desc root transaction Report TryResult
// @Param spanCtx
// @Param dtsAgentAddress some information of dtsAgent
// @Param compensableFlagSet
// @Param rootXid
// @Param branchXid
// @Return ok success or not
// @Return err error
func (d *DefaultTxnManager) ReportTryResult(spanCtx *compensable.SpanContext, dtsAgentAddress string, rootXid string, parentXid string, branchXid string, ok bool) (err error) {
	dtsAgentAddressElem := strings.Split(dtsAgentAddress, constant.PARTICIPANT_ADDRESS_SPLIT_CHAR)
	isDirectRequest := strings.EqualFold(constant.COMM_DIRECT, config.CmpSvrConfig.CommType)

	request := event.TxnTryResultReportRequest{
		Head: event.TxnEventHeader{
			Service: "ReportTransactionTryResult",
		},
		Request: event.TxnTryResultReportRequestBody{
			RootXid:     rootXid,
			BranchXid:   branchXid,
			ParentXid:   parentXid,
			Ok:          ok,
			RequestTime: util.CurrentTime(),
		},
	}

	//var response string
	var e error
	resBody := &event.TxnTryResultReportResponse{}
	//again:
	if isDirectRequest {
		e = d.RemoteClient.HttpPost(nil, dtsAgentAddressElem[0]+dtsAgentAddressElem[3], request, resBody, nil,
			config.CmpSvrConfig.DtsTimeoutMilliseconds,
			config.CmpSvrConfig.DtsMaxRetryTimes)
	} else {

		if len(dtsAgentAddressElem) != 7 {
			err = errors.Errorf("target address invalid, please check[%++v]", dtsAgentAddress)
			return
		}
		topicAttributes, _ := comm.BuildDTSTopicAttributes(
			dtsAgentAddressElem[0],
			dtsAgentAddressElem[1],
			dtsAgentAddressElem[2],
			dtsAgentAddressElem[3],
			dtsAgentAddressElem[6])

		e = d.RemoteClient.DTSRequestWithEvent(
			spanCtx,
			nil,
			topicAttributes,
			request,
			resBody,
			config.CmpSvrConfig.DtsTimeoutMilliseconds,
			config.CmpSvrConfig.DtsMaxRetryTimes,
		)
	}

	if e != nil {
		//goto again
		err = e
		return
	} else if 0 != resBody.ErrorCode {
		err = errors.Errorf("Report try result failed:[%s]!", resBody.ErrorMsg)
		return
	}

	return
}
