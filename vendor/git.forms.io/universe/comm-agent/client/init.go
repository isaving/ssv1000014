//
// Copyright 2019 Shenzhen Forms Syntron Information Co., Ltd. All rights reserved.
//

package client

import (
	log "git.forms.io/universe/comm-agent/common/log"
)

// InitSolAppClient provides solapp application init. when startup
// default callback listen on "0.0.0.0"(all IPv4 addresses on the local machine)
// callbackPort is for specify callback port
// commServerAddr is for specify address of comm-agent server(the format is "http://ip:port")
// callbackHandFunc represents the callback function, which needs to define the callback function at startup
// and pass the callback function to comm-sdk.
func InitSolAppClient(callbackPort int, commServerAddr string, callbackHandFunc func(UserMessage) (*UserMessage, error)) {
	log.Infof("Start client InitSolAppClient, callbackPort=[%d], commServerAddr[%s], callbackHandFunc=[%p]", callbackPort, commServerAddr, callbackHandFunc)
	SetCommServerAddr(commServerAddr)
	InitCommClient()
	CallbackFuncRegister(callbackHandFunc)
	go startUserMsgCallbackServer(callbackPort)
}
