package config

import (
	"git.forms.io/legobank/legoapp/constant"
	"git.forms.io/legobank/legoapp/encryption"
	"git.forms.io/legobank/legoapp/errors"
	"git.forms.io/legobank/legoapp/models"
	"git.forms.io/universe/comm-agent/client"
	"git.forms.io/universe/common/json"
	"git.forms.io/universe/solapp-sdk/config"
	"git.forms.io/universe/solapp-sdk/log"
	"github.com/astaxie/beego"
	"io/ioutil"
	"time"
)

var ServiceConf ServicesConfig

type ServicesConfig struct {
	Organization        string
	GroupDcn            string
	CommonDcn           string
	DataCenterNode      string
	DcnWithoutFirstByte string
	InstanceID          string
	ReqFormId           string
	RespFormId          string
	AppName             string
	TopicId             string
	Langs               string
	PaotangUseEncrypt   bool
	ServiceNo           int

	ParamTopic string
	ParamGroup string
	TxnParam   map[string]string
}

func InitServiceConfig() error {

	//init Solapp config
	if err := config.InitSolappConfig(); err != nil {
		log.Errorf("Init solapp config failed, err=%v", err)
		return err
	}

	//load service config
	if err := loadServiceConfig(); err != nil {
		log.Errorf("Load service config failed, %v", err)
	}

	//Load multi-language error message old function
	if err := InitLocaleLang(); err != nil {
		log.Errorf("Init I18n failed, err=%v", err)
		return err
	}

	//Load multi-language error message
	//if err := loadMultiMessage(); err != nil {
	//	log.Errorf("load multi-language failed, %v", err)
	//}

	//subscribe parameter
	ServiceConf.ParamTopic = beego.AppConfig.String("param::subTopic")
	ServiceConf.ParamGroup = beego.AppConfig.String("param::group")
	ServiceConf.TxnParam = make(map[string]string)

	if ServiceConf.ParamTopic != "" {
		if err := getTxnParam(); err != nil {
			panic(err)
		}
	}

	return nil
}

func loadServiceConfig() error {
	log.Info("Start init service config...")
	ServiceConf.GroupDcn = config.CmpSvrConfig.DstDcn
	ServiceConf.CommonDcn = beego.AppConfig.String("service::commonDcn")
	ServiceConf.Organization = config.CmpSvrConfig.DstOrg
	ServiceConf.AppName = config.CmpSvrConfig.AppName
	ServiceConf.DataCenterNode = config.CmpSvrConfig.DcnNo
	ServiceConf.InstanceID = config.CmpSvrConfig.InstanceID
	ServiceConf.DcnWithoutFirstByte = config.CmpSvrConfig.DcnWithoutFirstByte
	ServiceConf.ServiceNo = beego.AppConfig.DefaultInt("serviceNo", 0)

	log.Info("Init service config successfully.")

	if ServiceConf.CommonDcn == "" {
		panic("Can not found service.commonDcn")
	}

	//if err := InitLocaleLang(); err != nil {
	//	log.Errorf("Init I18n failed, err=%v", err)
	//	return err
	//}

	InitPaotangRSAPublicKey()

	//subscribe parameter
	ServiceConf.ParamTopic = beego.AppConfig.String("param::subTopic")
	ServiceConf.ParamGroup = beego.AppConfig.String("param::group")
	ServiceConf.TxnParam = make(map[string]string)

	if ServiceConf.ParamTopic != "" {
		if err := getTxnParam(); err != nil {
			panic(err)
		}
	}

	return nil
}

func InitPaotangRSAPublicKey() {

	paotangEncrypt := beego.AppConfig.DefaultBool("paotangSecurity::enableRSAEncrypt", false)
	if !paotangEncrypt {
		return
	}
	rsaPublicKeyPath := beego.AppConfig.String("paotangSecurity::RSAPublicKeyPath")
	if rsaPublicKeyPath == "" {
		panic("paotangSecurity.RSAPublicKeyPath not found")
	}
	rsaPublicKey, err := ioutil.ReadFile(rsaPublicKeyPath)
	if err != nil {
		panic("read RSA public key from " + rsaPublicKeyPath + " failed, " + err.Error())
	}

	if rsaPublicKey == nil {
		panic("RSA public key is empty")
	}

	encryption.GlobalPublicKey = rsaPublicKey

	ServiceConf.PaotangUseEncrypt = paotangEncrypt

}

func getTxnParam() error {

	topicAttribute, err := client.BuildBussinessTopicAttributes(ServiceConf.Organization, ServiceConf.GroupDcn, ServiceConf.ParamTopic)
	if nil != err {
		log.Errorf("Build business topic attributes failed:Org[%s],Dcn[%s],Service key[%s]", ServiceConf.Organization, ServiceConf.GroupDcn, ServiceConf.ParamTopic)
		return err
	}

	paramSub := models.ParamSub{
		ParamGroup: ServiceConf.ParamGroup,
	}

	bodyInfo := models.BodyMult{
		Form: []models.FormMult{
			{
				FormHead: models.FormHead{
					FormId: "",
				},
				FormData: paramSub,
			},
		},
	}

	body, err := json.Marshal(bodyInfo)

	if nil != err {
		log.Errorf("Build UserMessage body failed [%v]", err)
		return err
	}

	srcMap := make(map[string]string)
	srcMap[constant.SRCSYSID] = ServiceConf.InstanceID
	srcMap[constant.SRCDCN] = ServiceConf.DataCenterNode
	srcMap[constant.SRCSERVICEID] = ServiceConf.AppName
	srcMap[constant.SRCTIMESTAMP] = time.Now().Local().Format("2006-01-02 15:04:05.000")
	srcMap[constant.SRCTOPOICID] = ServiceConf.InstanceID
	srcMap[constant.SRCBIZDATE] = time.Now().Local().Format("20060102")

	request := &client.UserMessage{
		TopicAttribute: topicAttribute,
		AppProps:       srcMap,
		Body:           body,
	}

	log.Debugf("Start request %s, requestAppProps: [%++v], requestBody: [%v].", ServiceConf.ParamTopic, request.AppProps, string(request.Body))
	response, err := client.SendRequestMessage(request)
	if err != nil {
		log.Errorf("SendRequestMessage failed [%v]", err)
		return err
	}
	log.Debugf("End request %s, responseAppProps: [%++v], responseBody: [%v].", ServiceConf.ParamTopic, response.AppProps, string(response.Body))
	respMap := make(map[string]string)

	respMap, err = parseUserMessage(response)

	if err != nil {
		log.Errorf("Parse response message failed [%v]", err)
		return err
	}

	ServiceConf.TxnParam = respMap

	return nil
}

func parseUserMessage(response *client.UserMessage) (map[string]string, error) {

	respMap := make(map[string]string)
	respBody := &models.ParamResp{}
	if err := json.Unmarshal(response.Body, respBody); err != nil {
		return nil, err
	}

	if len(respBody.Form) < 1 {
		return nil, errors.New("UserMessage body illegal", "")
	}
	respMap = respBody.Form[0].FormData

	if response.AppProps[constant.RETSTATUS] == "F" {
		return nil, errors.New(respMap[constant.RETMESSAGE], "")
	}

	if respBody.Form[0].FormHead.FormId == constant.ERRORFORMID {
		return nil, errors.New(respMap[constant.RETMESSAGE], "")
	}

	return respMap, nil

}
