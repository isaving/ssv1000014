package util

import (
	"git.forms.io/universe/comm-agent/client"
	"unsafe"
)

type Message struct {
	Id             uint64
	TopicAttribute map[string]string
	NeedReply      bool
	NeedAck        bool
	SessionName    string
	AppProps       map[string]string
	Body           string
}

func PrintUserMsg(userMsg *client.UserMessage) Message {
	msg := Message{
		Id:             userMsg.Id,
		TopicAttribute: userMsg.TopicAttribute,
		NeedReply:      userMsg.NeedReply,
		NeedAck:        userMsg.NeedAck,
		SessionName:    userMsg.SessionName,
		AppProps:       userMsg.AppProps,
	}
	if len(userMsg.Body) > 0 {
		msg.Body = *(*string)(unsafe.Pointer(&userMsg.Body))
	}
	return msg
}
